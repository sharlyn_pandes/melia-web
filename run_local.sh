#!/usr/bin/env bash

function error {
    echo "ERROR: target env config not found" ; exit 1;
}

cp app/constant.local.js app/constant.js || error

./node_modules/.bin/webpack-dev-server --config webpack.config.dev.js --content-base app --inline --hot --port 8000
