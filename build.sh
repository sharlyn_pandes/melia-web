#!/usr/bin/env bash

function error {
    echo "ERROR: target env config not found" ; exit 1;
}

cp app/constant.deploy.js app/constant.js || error

./node_modules/.bin/webpack --config webpack.config.prod.js --content-base app
