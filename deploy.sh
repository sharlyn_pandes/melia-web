#!/usr/bin/env bash

function usage {
    cat << EOF
Usage: ./deploy.sh <environment> <version>
EOF
    exit 1
}

TARGET_ENV="$1"
TARGET_VERSION="$2"

if [ $# -gt 2 ] || [ $# -lt 2 ]; then
    usage;
fi

./build.sh

gcloud app deploy --project=${TARGET_ENV} --version=${TARGET_VERSION} dist/app.yaml --no-promote
