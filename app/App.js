import angular from 'angular';
import 'angular-ui-bootstrap';
import './Login/LoginModule';
import './CampaignPerformance/CampaignPerformanceModule';
import './BriefingReport/BriefingReportModule';
import './ModelControlPanel/ModelPanelModule';
import './Dashboard/DashboardModule';
import './CampaignList/CampaignListModule';
import './static/css/bootstrap.min.css';
import './static/css/rdash.min.css';
import './static/css/font-awesome.min.css';
import './static/css/custom_main.css';
import API_CONSTANTS from './constant';
import Routes from './Routes';


angular.module('App', [
  'ui.bootstrap',
  'LoginModule',
  'DashboardModule',
  'CampaignListModule',
  'CampaignPerformanceModule',
  'BriefingReportModule',
  'ModelPanelModule'
  ])
  .constant('API_CONFIG', API_CONSTANTS)
  .config(Routes);
