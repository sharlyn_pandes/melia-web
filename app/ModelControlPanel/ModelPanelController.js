export class ModelPanelController {

    constructor(DashboardService) {
        this.DashboardService = DashboardService;
        this.load();
    }

    load() {
        this.DashboardService.setHeaderLabel("Don't Stop Moving Campaign Model Control Panel");
        console.log("hello from ModelControlPanelController");
        this.minimumThreshold = this.getMinimumThreshold();
        this.thresholdList = this.getThresholdList();
        this.daysList = this.getDays();
        this.audienceList = this.getAudienceList();
        console.log(this.audienceList);
    }

    getMinimumThreshold() {
        var data = {"threshold": [2,3,4,5,6,7,8,9]};
        return data;
    }

    getThresholdList() {
        var data = [
            {"id": "1",
            "creative_1_morning":4.1,
            "creative_1_evening":2.8,
            "creative_2_morning":3.2,
            "creative_2_evening":3.0,
            "top_score":4.1},

            {"id": "2",
            "creative_1_morning":0.3,
            "creative_1_evening":3.8,
            "creative_2_morning":3.3,
            "creative_2_evening":3.1,
            "top_score":3.8}
        ];
       return data;
    }

    getDays() {
        var data = {"campaign_day":[ "1st", "2nd", "3rd", "4th", "5th", "6th", "7th"]};
        return data;
    }

    getAudienceList() {
        var data = [
            {"id":"1", "age":23, "state":"CA", "affinity":"Are and Theater Aficionados", "search_keywords":"Latest Movies", "no_of_contacts":0},
            {"id":"2", "age":43, "state":"AL", "affinity":"Auto Enthusiasts", "search_keywords":"BMW 2017", "no_of_contacts":0},
            {"id":"3", "age":42, "state":"AL", "affinity":"Avid Investors", "search_keywords":"Tech Trend", "no_of_contacts":1},
            {"id":"4", "age":38, "state":"FL", "affinity":"Beauty Mavens", "search_keywords":"Estee Lauder", "no_of_contacts":1},
            {"id":"5", "age":57, "state":"TX", "affinity":"Business Professionals", "search_keywords":"Google Finance", "no_of_contacts":2},
            {"id":"6", "age":65, "state":"MS", "affinity":"Comic and Animation Film", "search_keywords":"Iron Man Movies", "no_of_contacts":3},
            {"id":"7", "age":40, "state":"FL", "affinity":"Cooking Enthusiasts", "search_keywords":"Korean Barbecue", "no_of_contacts":3},
            {"id":"8", "age":59, "state":"NJ", "affinity":"Do-It-Yourselfers", "search_keywords":"IKEA store by me", "no_of_contacts":4},
            {"id":"9", "age":34, "state":"MA", "affinity":"Family-focused", "search_keywords":"Jumping house hours", "no_of_contacts":4},
            {"id":"10", "age":41, "state":"FL", "affinity":"Fashionistas", "search_keywords":"Macy new lines", "no_of_contacts":5},
        ];
        return data;
    }
}

ModelPanelController.$inject = [ 'DashboardService' ];