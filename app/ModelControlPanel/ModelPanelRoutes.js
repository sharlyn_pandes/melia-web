export default function ModelPanelRoutes($stateProvider) {
  $stateProvider
    .state({
      name: 'campaign.modelPanel',
      url: '/model_panel',
      template: '<model-panel/>'
    });
}

ModelPanelRoutes.$inject = [ '$stateProvider' ];