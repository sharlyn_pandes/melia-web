import angular from 'angular';
import 'angular-ui-router';
import ModelPanelRoutes from './ModelPanelRoutes';
import ModelPanelComponent from './ModelPanelComponent';

angular.module('ModelPanelModule', ['ui.router'])
  .component('modelPanel', ModelPanelComponent)
  .config(ModelPanelRoutes);
