import modelPanelTemplate from './modelPanel.template.html';
import {ModelPanelController} from './ModelPanelController';
export default {

  template: modelPanelTemplate,
  controller: ModelPanelController
}