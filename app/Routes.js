export default function Routes($urlRouterProvider) {
  $urlRouterProvider.otherwise('/login');
}

Routes.$inject = [ '$urlRouterProvider' ];