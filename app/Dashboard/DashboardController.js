export class DashboardController {
  constructor(DashboardService) {
    this.DashboardService = DashboardService;
    this.load();

  }

  load() {
    this.toggle = true;
    console.log("loaded dashboard controller")
  }

  toggleSidebar() {
    console.log("toggled sidebar:", this.toggle)
    this.toggle = !this.toggle;
  }

  getHeaderLabel() {
    return this.DashboardService.getHeaderLabel();
  }


}

DashboardController.$inject = [ 'DashboardService' ];