"use strict";

export class DashboardService {

  constructor() {
    this.headerLabel = "";
  }

  setHeaderLabel(label) {
    this.headerLabel = label;
  }

  getHeaderLabel() {
    return this.headerLabel;
  }

}

DashboardService.$inject = [];