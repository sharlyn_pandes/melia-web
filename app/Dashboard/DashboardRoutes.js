export default function DashboardRoutes($stateProvider) {
  $stateProvider
    .state({
      name: 'campaign',
      url: '/campaign',
      abstract: true,
      template: '<dashboard/>',

    });
}

DashboardRoutes.$inject = [ '$stateProvider' ];