import angular from 'angular';
import 'angular-ui-router';
import BriefingReportRoutes from './BriefingReportRoutes';
import BriefingReportComponent from './BriefingReportComponent';

angular.module('BriefingReportModule', ['ui.router'])
  .component('briefingReport', BriefingReportComponent)
  .config(BriefingReportRoutes);
