import briefingReportTemplate from './briefingReport.template.html';
import {BriefingReportController} from './BriefingReportController';
export default {

  template: briefingReportTemplate,
  controller: BriefingReportController
}