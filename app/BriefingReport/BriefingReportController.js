export class BriefingReportController {

    constructor(DashboardService) {
        this.DashboardService = DashboardService;
        this.load();
    }

    load() {
        console.log("hello from BriefingReportController");
        this.tblMarketingBrief = this.getMarketingBrief();
        this.DashboardService.setHeaderLabel("Don't Stop Moving Campaign Briefing Report");
    }

    getMarketingBrief() {
        var data = [
            {"owner":"John Smith",
            "start_date":"01/01/2017",
            "end_date":"01/02/2017",
            "media_type":"youtube",
            "no_of_creative":"2",
            "creative_1":"Description here",
            "creative_2":"Description here",
            "budget":"$$$",
            "target_audience_size":1000,
            "cpm":"10%",
            "expected_view_rate":"50%",
            "expected_click_through_rate":"10%",
            "expected_roi":"15-17%",
            "targeting_dataset_name":"XXXX"}
        ];
        return data;
    }
}

BriefingReportController.$inject = [ 'DashboardService' ];