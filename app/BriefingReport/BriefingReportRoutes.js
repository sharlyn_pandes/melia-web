export default function BriefingReportRoutes($stateProvider) {
  $stateProvider
    .state({
      name: 'campaign.report',
      url: '/report',
      template: '<briefing-report/>'
    });
}

BriefingReportRoutes.$inject = [ '$stateProvider' ];