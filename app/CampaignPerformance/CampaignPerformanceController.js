export class CampaignPerformanceController {

    constructor(DashboardService) {
        console.log("CampaignPerformanceController Loaded");
        this.DashboardService = DashboardService;
        this.load();

    }

    load() {
        this.DashboardService.setHeaderLabel("Don't Stop Moving Campaign Performance Page");
        this.tblCampaignPerformance = this.getCampaignPerformance();
        this.tblVideoCampaignPlacement = this.getVideoCampaignPlacement();
        this.filters = this.getFilters();

    }

    getCampaignPerformance() {
        var data = [
            {"media": "Youtube",
            "campaign": "Don't stop Moving",
            "conversion_performance" : "^",
            "conversion_rate": "0.4%",
            "view_rate":"4.0%",
            "ctr": "1.0%",
            "impression": 1000.01009}
        ];
        return data;
    }

    getVideoCampaignPlacement() {
        var data = [
            {"placement":"Mobile Video",
            "view":92},
            {"placement":"Mobile Video",
            "view":92},
            {"placement":"Mobile Video",
            "view":92},
            {"placement":"Mobile Video",
            "view":92},
            {"placement":"Mobile Video",
            "view":92}
        ];
        return data;
    }

    getFilters() {
        var data = [
            {"days": ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]},
            {"months": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]},
            {"years": [2015, 2016, 2017] } ,
            {"quarters": ["1st", "2nd", "3rd", "4th"]},
            {"regions": ["Africa", "North America", "South America", "Asia", "Europe", "Oceania", "Polar"]}
        ];
        return data;
    }

}

CampaignPerformanceController.$inject = [ 'DashboardService' ];