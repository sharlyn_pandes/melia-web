export default function CampaignPerformanceRoutes($stateProvider) {
  $stateProvider
    .state({
      name: 'campaign.performance',
      url: '/performance',
      template: '<campaign-performance/>'
    });
}

CampaignPerformanceRoutes.$inject = [ '$stateProvider' ];