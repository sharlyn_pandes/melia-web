export default {
  API_NAME: "launchpad",
  API_VERSION: "v1",
  API_BASE: "https://launchpad-pto-dot-acn-2016-launchpad.appspot.com/_ah/api",
  CLIENT_ID: "587847684447-d76khq1qsv08iop72boeuqotame6ak08.apps.googleusercontent.com",
  SCOPES: [
    "email",
    "profile"
  ]
}

