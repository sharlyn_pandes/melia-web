import campaignListTemplate from './campaignList.template.html';
import {CampaignListController} from './CampaignListController';
export default {
  
  template: campaignListTemplate,
  controller: CampaignListController
}
