import angular from 'angular';
import 'angular-ui-router';
import CampaignListRoutes from './CampaignListRoutes';
import CampaignListComponent from './CampaignListComponent';

angular.module('CampaignListModule', ['ui.router'])
  .component('campaignList', CampaignListComponent)
  .config(CampaignListRoutes);
