export class CampaignListController {
  constructor(DashboardService) {
    this.DashboardService = DashboardService;
    this.load();
  }

  load() {
    console.log("loaded campaign list controller");
    this.DashboardService.setHeaderLabel("Campaign List");
    this.toggle = true;
    this.campaignList = [
      {
        "startDate" : "October 10",
        "campaignId" : "19349",
        "name" : "Don't Stop Moving Campaign",
        "media" : "Youtube",
        "goal" : "Learn more/Education"
      },
      {
        "startDate" : "October 20",
        "campaignId" : "19350",
        "name" : "Don't Stop Moving Campaign",
        "media" : "Youtube",
        "goal" : "Learn more/Education"
      },
      {
        "startDate" : "November 10",
        "campaignId" : "19351",
        "name" : "Don't Stop Moving Campaign",
        "media" : "Email",
        "goal" : "Promotional for new pricing"
      },
      {
        "startDate" : "November 20",
        "campaignId" : "19352",
        "name" : "Don't Stop Moving Campaign",
        "media" : "Facebook",
        "goal" : "Promotional for new pricing"
      }
    ];
    console.log("loaded dashboard controller");

  }

}

CampaignListController.$inject = [ 'DashboardService' ];
