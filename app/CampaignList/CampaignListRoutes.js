export default function CampaignListRoutes($stateProvider) {
  $stateProvider
    .state({
      name: 'campaign.list',
      url: '/list',
      template: '<campaign-list/>'
    });
}

CampaignListRoutes.$inject = [ '$stateProvider' ];