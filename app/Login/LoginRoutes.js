export default function LoginRoutes($stateProvider) {
  $stateProvider
    .state({
      name: 'login',
      url: '/login',
      template: '<login/>'
    });
}

LoginRoutes.$inject = [ '$stateProvider' ];