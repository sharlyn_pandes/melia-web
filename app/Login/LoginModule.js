import angular from 'angular';
import 'angular-ui-router';
import LoginRoutes from './LoginRoutes';
import LoginComponent from './LoginComponent';
import { LoginService } from './LoginService';
import { UserModel } from './UserModel'

angular.module('LoginModule', ['ui.router'])
  .component('login', LoginComponent)
  .config(LoginRoutes)
  .service("LoginService", LoginService);
  //.factory('UserModel', UserModel);

function init() {
  window.init();
}