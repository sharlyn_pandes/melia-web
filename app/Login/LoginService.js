"use strict";

import {UserModel} from './UserModel';

export class LoginService {

  constructor($q, API_CONFIG, $state) {
    this.$q = $q;
    this.API_CONFIG = API_CONFIG;
    this.$state = $state;
  }

  loadApi() {
    gapi.client.load(this.API_CONFIG.API_NAME, this.API_CONFIG.API_VERSION, function() {
      console.log("LOADED API");
    }, this.API_CONFIG.API_BASE);
  }

  login(){
    let deferred  = this.$q.defer();
    gapi.client.launchpad.sample().execute(function(resp) {
      deferred.resolve(resp);
    });
    return deferred.promise;
  }

  goToDashboard() {
    this.$state.go('campaign.list');
  }

  checkUser(user) {
    console.log("checking");
    let userModel = new UserModel(user);
    if(userModel.email == "user@user" && userModel.password == "password") {
      this.$state.go('campaign.list');
    }
  }

}

LoginService.$inject = [ '$q', 'API_CONFIG', '$state'];