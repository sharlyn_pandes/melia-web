export default function AppConfig($gapiProvider) {
  $gapiProvider.client_id = GAPI_CONFIG.CLIENT_ID;
  $gapiProvider.scopes = GAPI_CONFIG.SCOPES;
}

AppConfig.inject[ ''];